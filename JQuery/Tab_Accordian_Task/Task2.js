$(document).ready(function () {
    $(".content_section").hide().first().show();
    $(".btn").first().find("i").removeClass("fa-caret-right").addClass("fa-caret-down");
    $(".btn").click(function () {
        $(".content_section").slideUp(function () {
            if (!$('.content_section').is(':visible')) {
                $(this).parent().find('.btn i').removeClass("fa-caret-down").addClass("fa-caret-right");
            }
        });

        $(this).find('i').removeClass("fa-caret-right").addClass("fa-caret-down");
        $(this).parent().siblings().find('.btn i').removeClass("fa-caret-down").addClass("fa-caret-right");
        $(this).siblings('p').show();
    });
})