$(document).ready(function () {
    $(".content_section").not($(".content_section").eq(0)).hide();
    $(".btn").click(function () {
        var i = $(this).index();
        $(".content_section").not($(".content_section").eq(i).show()).hide();
    });
});