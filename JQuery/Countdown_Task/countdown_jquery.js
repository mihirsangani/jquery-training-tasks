$(document).ready(function () {


    // Hide Events
    $(".resume").hide();
    $(".restart").hide();
    $("span").hide();
    $(".present_condition").hide().first().show();

    // Global Variable
    var timer;
    $(".buttonBar").click(function () {
        $(".reset").attr('disabled', false);
    });
    // Start Event
    $('.start').click(function () {
        var inputValue = $('.timeToCountdown').val();
        console.log(inputValue);
        if (inputValue == "") {
            return;
        }

        $(".present_condition").not($(".present_condition").eq(3).show()).hide();
        startRestartFunction();
        $(".pause").attr('disabled', false);
        $(".stop").attr('disabled', false);
        $(".start").attr('disabled', true);

    });

    // Stop Event
    $('.stop').click(function () {
        $(".present_condition").not($(".present_condition").eq(1).show()).hide();
        $(".resume").hide();
        $("span").hide();
        $(".start").hide();
        $(".restart").show();
        clearInterval(timer);
        $(".restart").attr('disabled', false);
        $(".pause").attr('disabled', true);
        $(".stop").attr('disabled', true);
    });

    // Restart Event
    $('.restart').click(function () {
        $(".present_condition").not($(".present_condition").eq(3).show()).hide();
        $(".resume").hide();
        $("span").hide();
        $(".start").show();
        $(".restart").hide();
        startRestartFunction();
        $(".pause").attr('disabled', false);
        $(".stop").attr('disabled', false);
        $(".restart").attr('disabled', true);
    })

    // Pause Event
    $('.pause').click(function () {
        h = parseInt($("#hr").text());
        m = parseInt($("#min").text());
        s = parseInt($("#sec").text());
        c = parseInt($("#cSec").text());
        pauseAt = h + " HH, " + m + " MM, " + s + " SS.";
        console.log(pauseAt);
        $("span").text(pauseAt);
        $(".present_condition").not($(".present_condition").eq(2).show()).hide();
        $("span").show();
        $(".start").hide();
        $(".restart").hide();
        $(".resume").show();
        console.log(hr, min, sec, cSec);
        clearInterval(timer);
        $(".resume").attr('disabled', false);
        $(".stop").attr('disabled', true);
        $(".pause").attr('disabled', true);
    });

    // Resume Event
    $('.resume').click(function () {
        $(".present_condition").not($(".present_condition").eq(3).show()).hide();
        $(".resume").hide();
        $(".restart").hide();
        $("span").hide();
        $(".start").show();
        $("th").eq(0).text(h);
        $("th").eq(1).text(m);
        $("th").eq(2).text(s);
        $("th").eq(3).text(c);
        intervalFunction(h, m, s, c);
        $(".stop").attr('disabled', false);
        $(".pause").attr('disabled', false);
        $(".resume").attr('disabled', true);
    });

    // Reset Event
    $('.reset').click(function () {
        $(".present_condition").hide().first().show();
        $(".resume").hide();
        $(".start").show();
        $(".restart").hide();
        $("span").hide();
        clearInterval(timer);
        $("th").text("00");
        $(".timeToCountdown").val("");
        $(".start").attr('disabled', false);
        $(".stop").attr('disabled', true);
        $(".pause").attr('disabled', true);
        $(".reset").attr('disabled', true);
    });

    // Interval Function
    function intervalFunction(h, m, s, c) {
        timer = setInterval(function () {
            if (c > 0) {
                c--;
            } else {
                c = 99;
                if (s > 0) {
                    s--;
                } else {
                    s = 59;
                    if (m > 0) {
                        m--;
                    } else {
                        m = 59
                        if (h > 0) {
                            h--;
                        } else {
                            h, m, s, c = 0;
                        }
                    }
                }
            }
            if (h == 0 && m == 0 && s == 0 && c == 0) {
                clearInterval(timer);
            }
            $("th").eq(0).text(h);
            $("th").eq(1).text(m);
            $("th").eq(2).text(s);
            $("th").eq(3).text(c);
        }, 10);
    }

    // Start / Restart Function
    function startRestartFunction() {
        var inputValue = $('.timeToCountdown').val();
        var h, m, s, c = 0;
        h = Math.floor(inputValue / 3600);
        m = Math.floor((inputValue - (h * 3600)) / 60);
        s = Math.floor((inputValue - (h * 3600) - (m * 60)));
        c = 0;
        $("th").eq(0).text(h);
        $("th").eq(1).text(m);
        $("th").eq(2).text(s);
        $("th").eq(3).text(c);
        intervalFunction(h, m, s, c);
    }
});