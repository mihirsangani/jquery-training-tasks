$(document).ready(function () {
    $(".resume,.restart").hide();
    $(".present_condition").hide().first().show();
    var timer;
    var h, m, s, c = 99;

    // Start Event
    $(".start").click(function () {
        $(".present_condition").not($(".present_condition").eq(3).show()).hide();
        $(".pause,.stop,.reset").attr('disabled', false);
        startRestartInterval();
    });

    // Stop Event
    $(".stop").click(function () {
        $(".present_condition").not($(".present_condition").eq(1).show()).hide();
        $(".pause,.stop").attr('disabled', true);
        $(".reset,.restart").attr('disabled', false);
        $(".start,.resume").hide();
        $(".restart").show();
        getValues();
        pauseAt = "STOPPED AT: " + h + " HH, " + m + " MM, " + s + " SS.";
        $("#stopTimeStamp").html("<p>" + pauseAt + "</p>");
        clearInterval(timer);
    });

    // Restart Event
    $(".restart").click(function () {
        $("#pauseTimeStamp,#stopTimeStamp").empty();
        $(".present_condition").not($(".present_condition").eq(3).show()).hide();
        h = m = s = c = 0;
        startRestartInterval();
        $(".pause,.stop").attr('disabled', false);
        $(".start").attr('disabled', true);
        $(".start").show();
        $(".resume,.restart").hide();
    });

    // Pause Event
    $(".pause").click(function () {
        getValues();
        pauseAt = "PAUSE AT: " + h + " HH, " + m + " MM, " + s + " SS.";
        $(".pauseValue").css("color", "blue").text(pauseAt);
        $("#pauseTimeStamp").append("<p>" + pauseAt + "</p>");
        $(".present_condition").not($(".present_condition").eq(2).show()).hide();
        $(".start,.restart").hide();
        $(".resume").show();
        $(".pause").attr('disabled', true);
        $(".resume").attr('disabled', false);
        clearInterval(timer);
    });

    // Resume Event
    $(".resume").click(function () {
        getValues();
        startRestartInterval();
        $(".start").show();
        $(".resume,.restart").hide();
        $(".pause,.stop").attr('disabled', false);
        $(".start").attr('disabled', true);
        $(".present_condition").not($(".present_condition").eq(3).show()).hide();
    });

    // Reset Event
    $(".reset").click(function () {
        clearInterval(timer);
        $("#pauseTimeStamp,#stopTimeStamp").empty();
        $("th").text("00");
        $(".start").show();
        $(".resume,.restart").hide();
        $(".pause,.stop,.reset").attr('disabled', true);
        $(".start").attr('disabled', false);
        $(".present_condition").hide().first().show();
    });

    // Functions
    function startRestartInterval() {
        timer = setInterval(function () {
            if (c < 99) {
                c++;
                console.log(c);
            }
            else {
                c = 0;
                if (s < 59) {
                    s++;
                    console.log(s);
                } else {
                    s = 0;
                    if (m < 59) {
                        m++;
                        console.log(m);
                    } else {
                        m = 0;
                        if (h < 59) {
                            h++;
                            console.log(h);
                        }
                    }
                }
            }
            $("th").eq(0).text(h);
            $("th").eq(1).text(m);
            $("th").eq(2).text(s);
            $("th").eq(3).text(c);
        }, 10);
    }
    function getValues() {
        h = parseInt($("#hr").text());
        m = parseInt($("#min").text());
        s = parseInt($("#sec").text());
        c = parseInt($("#cSec").text());
    }
});