$(document).ready(function () {

    // JQuery HTML GET
    $("#alert_message_with_text").click(function () {
        alert("Text: " + $("#text").text());
    });

    $("#alert_message_with_html").click(function () {
        alert("HTML: " + $("#text").html());
    });

    $("#alert_message_with_val").click(function () {
        alert("Value: " + $("#name").val());
    });

    $("#attributes").click(function () {
        alert($("#link").attr("href"));
    });

    $("#changeAttr").click(function () {
        // $("#link1").attr("href","https://www.instagram.com/").attr("title","Instagram");
        $("#link1").attr({
            "href": "https://www.instagram.com/",
            "title": "Instagram"
        });
    });


    // JQuery HTML SET
    $("#set_new_show_old_text").click(function () {
        $("#text1").text(function (i, orig) {
            return "Old Text : " + orig + " New Text : Hello World! (index: " + i + ")";
        });
    });

    $("#set_new_show_old_html").click(function () {
        $("#text2").html(function (i, orig) {
            return "Old Text : " + orig + " Net Text : Hello <b>World!</b> (index: " + i + ")";
        });
    });


    // JQuery Append Prepend After Before
    $("#append_p").click(function () {

        $("#text3").append("<b> Hiii...</b>");
        $("#text3").prepend("<b> Hiii...</b>");

        $("#image_append").after("<img src='./Images/scroller.png'>");
        $("#image_append").before("<img src='./Images/scroller.png'>");

    });

    $("#append_li").click(function () {
        $("#list").append("<li>Append Items</li>");
        $("#list").prepend("<li>Append Items</li>");
    });


    // JQuery Remove Empty
    $("#removeElement").click(function () {
        $("#my_text").remove();
        // $("#bold_element").remove();
    });

    $("#emptyElement").click(function () {
        $("#my_text").empty();
    })


    // JQuery addClass removeClass toggleClass css
    $("#add_class").click(function () {
        $("#text5").addClass("blue");
        $("#text6").addClass("important");
        $("#text7").addClass("important blue");
        $(".css_classes").addClass("bg_yellow_brd_black");
    });

    $("#remove_class").click(function () {
        $("#text5").removeClass("blue");
        $("#text6").removeClass("important");
        $("#text7").removeClass("important blue");
        $(".css_classes").removeClass("bg_yellow_brd_black");
    });

    $("#toggle_class").click(function () {
        $("#text5").toggleClass("blue");
        $("#text6").toggleClass("important");
        $("#text7").toggleClass("important blue");
        $(".css_classes").toggleClass("bg_yellow_brd_black");
    });

    $("#return_css").click(function () {
        alert("Color = " + $("#text5, #text6, #text7").css("color"));
    });

    $("#change_css").click(function () {
        $(".css_classes p").css({
            "color": "red",
            "font-size": "36px"
        });
    });

    $("#dmns_of_selected_element").click(function () {
        var txt = "";
        txt += "Width of div: " + $(".dimension_details").width() + "</br>";
        txt += "Inner Width of div: " + $(".dimension_details").innerWidth() + "</br>";
        txt += "Outer Width of div: " + $(".dimension_details").outerWidth() + "</br>";
        txt += "Margin + Outer Width of div: " + $(".dimension_details").outerWidth(true) + "</br></br>";
        txt += "Height of div: " + $(".dimension_details").height() + "</br>";
        txt += "Inner Height of div: " + $(".dimension_details").innerHeight() + "</br>";
        txt += "Outer Height of div: " + $(".dimension_details").outerHeight() + "</br>";
        txt += "Margin + Outer Height of div: " + $(".dimension_details").outerHeight(true);
        $(".dimension_details").html(txt);
    });

    $("#dmns_of_screen").click(function(){
        var txt = "";
        txt += "Document Size: " + $(document).width();
        txt += " X " + $(document).height() + "<br>";
        txt += "Window Size: " + $(window).width();
        txt += " X " + $(window).height() + "<br>";
        $(".window_dimension_details").html(txt);
    });


});