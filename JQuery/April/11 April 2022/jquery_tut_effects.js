$(document).ready(function () {

    // JQuery Hdie / Show
    $("#show_link_when_click").hide();

    $("#hide").click(function () {
        $("#hide_show_element").hide(1000);
        $("a[target='blank'").hide();
    });

    $("#show").click(function () {
        $("#hide_show_element").show(1000);
        $("[href]").show();
    });

    $("#hide_show_element").click(function () {
        $("#hide").hide();
        $("#show").hide();
    });


    // Callback Function - Jump to next process only after the completion of previous process
    $("#hide_me").click(function () {
        $("#hide_me").hide(1000, function () {    // Also instead of "#hide_me" => ":button"(i.e: type = button) hide all type equals to button
            alert("The \"Hide Me\" button is now hidden")
        });
    });


    // JQuery Mouse Events
    $("#mouseenter").mouseenter(function () {
        // alert("Mouse Entered to <p> area.");
    });

    $("#mouseleave").mouseleave(function () {
        // alert("Mouse Leaved from <p> area.");
    });


    // JQuery Toggle Events
    $(".toggle_button").click(function () {
        $("p").toggle(1500);
    });

    $(document).ready(function () {
        $(".fadetoggle_button").click(function () {
            $("p").fadeToggle(1000);
        });
    });

    $(document).ready(function () {
        $(".fadeto_button").click(function () {
            $("p").fadeTo(1000, 0.4);
        });
    });

    $(".slidetoggle_button").click(function () {
        $("p").slideToggle();
    });


    // Animations
    $(".animations_button").click(function () {
        $("p").animate({
            fontSize: '+=10px'
            // paddingLeft: '+=5px'
        });
    });


    $(".animate_and_reset").click(function () {
        var div = $(".box");
        div.animate({ height: '300px', opacity: '0.4' }, "slow");
        div.animate({ width: '300px', opacity: '0.8' }, "slow");
        div.animate({ height: '100px', opacity: '0.4' }, "slow");
        div.animate({ width: '100px', opacity: '0.8' }, "slow");
    });


    // Stop
    $("#slidetoggle_for_stop").click(function () {
        $(".stopSlide").slideToggle(2000);
    });
    $("#stop_sliding").click(function () {
        $(".stopSlide").stop();
    });


    // JQuery Chaining
    $("#jq_chaining").click(function () {
        $(".jquery_chaining").css("color", "red").slideUp(2000).slideDown(2000).animate({ width: "100%" }, 2000).animate({ width: "0%" }, 4000).animate({ width: "500px" }, 2000);
    });
});